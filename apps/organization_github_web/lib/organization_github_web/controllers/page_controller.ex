defmodule OrganizationGithubWeb.PageController do
  use OrganizationGithubWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def git_pull(conn, %{"organization" => organization}) do
    organization = OrganizationGithub.organization_name(organization)
    org = organization |> elem(1)
    org = if is_list(org) do
      render conn, "show.html", organization: org
    else
      render conn, "no_show.html", organization: org
    end
  end
end
