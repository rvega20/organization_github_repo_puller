defmodule OrganizationGithubWeb.Router do
  use OrganizationGithubWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", OrganizationGithubWeb do
    pipe_through :browser

    get "/", PageController, :index
    post "/", PageController, :git_pull
  end

  # Other scopes may use custom stacks.
  # scope "/api", OrganizationGithubWeb do
  #   pipe_through :api
  #
  #   scope "/v1" do
  #     get "/repos", RepoController, :index
  #   end
  # end
end
