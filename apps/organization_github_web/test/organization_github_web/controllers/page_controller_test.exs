defmodule OrganizationGithubWeb.PageControllerTest do
  use OrganizationGithubWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Enter Organization name!"
  end

  # Test that the repos are gotten
  describe "validate_that_github_api_data" do
    test "should get api data" do
      company = "scriptdrop"
      organization = OrganizationGithub.organization_name(company)
      assert organization |> elem(0) == :ok
    end

    test "should show message if no organization found" do
      company = "zztop"
      organization = OrganizationGithub.organization_name(company)
      no_org = organization |> elem(1)
      assert no_org["message"] == "Not Found"
    end
  end
end
