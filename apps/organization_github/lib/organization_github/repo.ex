defmodule OrganizationGithub.Repo do
  use Ecto.Repo,
    otp_app: :organization_github,
    adapter: Ecto.Adapters.Postgres
end
