defmodule OrganizationGithub do
  @moduledoc """
  OrganizationGithub keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """
    def make_request(url, method) do
      apply(HTTPoison, method, [url])
    end

    def make_request(url, method, body) do
      apply(HTTPoison, method, [url, body])
    end

    def organization_name(organization) do
      case get_organization_url(organization) do
        {:ok, response} ->
      case decode_response(response.body) do
        %{"error" => error_messages} ->
          {:error, decode_response(response.body)}
        _ ->
          {:ok, decode_response(response.body)}
      end
    {:error, response} ->
      {:error, decode_response(response.body)}
      end
    end

    defp get_organization_url(organization) do
      github_api_v3 = "https://api.github.com"
      "#{github_api_v3}/orgs/#{organization}/repos"
      |> make_request(:get)
    end

    defp decode_response(body) do
      {:ok, body} = Poison.decode(body)
      body
    end

end
